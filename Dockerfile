FROM golang:1.14 as builder
#作者
MAINTAINER liyuan.zhang "liyuan.zhang@daocloud.io"

## 在docker的根目录下创建相应的使用目录
RUN mkdir -p /www/webapp
## 设置程序在容器内的工作路径
WORKDIR /www/webapp
COPY . /www/webapp

# 这里在docker里也使用go module的代理服务
ENV GO111MODULE=on
ENV GOPROXY="https://goproxy.io"
# skywalking配置
#ARG DAOSHOP_LIKE_SERVER
#ARG SERVICE_INSTANCE_NAME
#ARG DX_DMP_TRACING_SERVER
#ARG DX_ENV_ID
#ARG SW_AGENT_KUBE_NS
#ARG DX_SERVICE_NAME
#ENV DX_DMP_TRACING_SERVER=${DX_DMP_TRACING_SERVER} \
#    DX_ENV_ID=${DX_ENV_ID} \
#    SW_AGENT_KUBE_NS=${SW_AGENT_KUBE_NS} \
#    DX_SERVICE_NAME=${DX_SERVICE_NAME} \
#    DAOSHOP_LIKE_SERVER=${DAOSHOP_LIKE_SERVER} \
#    SERVICE_INSTANCE_NAME=${SERVICE_INSTANCE_NAME}
## 编译
RUN go build .
## 暴露容器内部端口
EXPOSE 8081
## 启动docker需要执行的文件
CMD go run main.go