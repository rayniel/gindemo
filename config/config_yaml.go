package config

/**
 * @author  liyuan.zhang
 * @date  2021/2/24 17:52
 */
type Config struct {
	System System `yaml:"system"`
	Db     Db     `yaml:"db"`
	Redis  Redis  `yaml:"redis"`
}
type System struct {
	DeBug bool `yaml:"isDebug"`
}

type Db struct {
	Host        string `yaml:"host"`
	Port        int    `yaml:"port"`
	InitialDb   string `yaml:"initialDb"`
	Username    string `yaml:"username"`
	Password    string `yaml:"password"`
	MaxIdleConn int    `yaml:"max-idle-conn"`
	MaxOpenConn int    `yaml:"max-open-conn"`
	Logmode     bool   `yaml:"logmode"`
}
type Redis struct {
	Addr     string `yaml:"addr"`
	Password string `yaml:"password"`
	Db       int    `yaml:"db"`
	Port     string `yaml:"port"`
	Size     int    `yaml:"size"`
}
