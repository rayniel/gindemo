package model

/**
 * @author  liyuan.zhang
 * @date  2021/2/24 17:35
 */

type User struct {
	Id       int    `json:"id"`
	UserName string `json:"username"`
	Password string `json:"password"`
}
