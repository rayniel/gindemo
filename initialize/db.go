package initilize

import (
	"log"

	"daocloud.io/dmp/demo/global"
	"daocloud.io/dmp/demo/model"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

/**
 * @author  liyuan.zhang
 * @date  2021/2/24 17:49
 */

func init() {
	dbConfig := global.CONFIG.Db
	// 这里使用sqllite3数据库，如果要使用mysql 使用global.CONFIG.Db参数拼接一下就好了
	if db, err := gorm.Open("sqlite3", "./tmp/comment.db"); err != nil {
		log.Println("数据库连接异常")
	} else {
		global.DB = db
		global.DB.DB().SetMaxIdleConns(dbConfig.MaxIdleConn)
		global.DB.DB().SetMaxOpenConns(dbConfig.MaxOpenConn)
		// 开启日志
		global.DB.LogMode(dbConfig.Logmode)
		initializeTable()
	}
}

/**初始化数据库操作*/
func initializeTable() {
	//表不存在就创建表
	if !global.DB.HasTable("user") {
		//创建表
		global.DB.AutoMigrate(&model.User{})
	}
}
