package initilize

import (
	//"github.com/go-redis/redis"
	//"log"
	//"daocloud.io/dmp/demo/global"
	//"github.com/garyburd/redigo/redis"
	"daocloud.io/dmp/demo/global"
	"github.com/garyburd/redigo/redis"
	"log"
)

/**
 * @author  liyuan.zhang
 * @date  2021/3/8 10:17
 * 初始化redis，并赋值给全局变量
 */

func init() {
	//// redisConfig := global.CONFIG.Redis;
	conn, err := redis.Dial("tcp", "localhost:6379")
	if err != nil {
		log.Println("redis连接异常")
		return
	}
	global.REDISDB = conn
}
