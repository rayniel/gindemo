package initilize

import (
	"daocloud.io/dmp/demo/config"
	"daocloud.io/dmp/demo/global"
	"fmt"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
)

/**
 * @author  liyuan.zhang
 * @date  2021/2/25 15:25
 * 读取配置文件,最先被初始化
 */

func init() {
	yamlFile, err := ioutil.ReadFile("config/application.yaml")
	if err != nil {
		log.Panicf("Failed to read file , cause is %s", err.Error())
	}
	config := config.Config{}
	err = yaml.Unmarshal(yamlFile, &config)
	global.CONFIG = config
	if err != nil {
		fmt.Println(err.Error())
	}
}
