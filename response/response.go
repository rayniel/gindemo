package response

import "net/http"

/**
 * @author  liyuan.zhang
 * @date  2021/2/24 22:33
 */

type Response struct {
	Code  int         `json:"code,omitempty"`
	Data  interface{} `json:"data,omitempty"`
	Msg   string      `json:"msg,omitempty"`
	Error string      `json:"error,omitempty"`
}

/*数据操作失败返回结果*/
func DataBaseFail(err error) Response {
	return Response{
		Code:  http.StatusInternalServerError,
		Msg:   "Database query failed",
		Error: err.Error(),
	}
}
