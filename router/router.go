package router

import (
	"daocloud.io/dmp/demo/controller"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

/**
 * @author  liyuan.zhang
 * @date  2021/2/24 17:20
 */

// 初始化路由
func InitRouters() *gin.Engine {
	// 创建默认带中间件的路由 Logger、Recovery
	r := gin.Default()
	// 允许跨域
	r.Use(cors.Default())
	user := r.Group("/user/v1")
	{
		user.GET("/user/:userId", controller.FindUserById)
		user.POST("/user", controller.AddUser)
		user.GET("/users", controller.FindAllUser)
	}
	// 测试Redis
	r.GET("/redis", controller.TestRedis)
	return r
}
