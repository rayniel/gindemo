### 探索一下基于gin的开发方式

- web 使用gin框架
- orm 使用gorm框架
- 提供DockerFile文件

#### 代码结构
```
demo 
   ├──config        配置文件以及配置类
   ├──controller    控制器
   ├──globel        全局变量
   ├──initialize    初始化数据库操作和初始化配置文件
   ├──model         数据模型
   ├──response      统一响应结果
   ├──router        路由配置
   ├──service       数据库操作
   ├──tmp           数据库临时文件，因为demo没有使用mysql
   ├──util          工具类
   ├──Dockerfile    DockerFile文件
   ├──go.mod        go.mod
   ├──main.go       启动类

```
#### 测试接口
**提供三个测试接口**
- 查询所有 GET

```localhost:8080/user/v1/user/2```

- 通过id查询GET

```localhost:8080/user/v1/users```

- 保存用户
 
```localhost:8080/user/v1/user```


> 参考了一些项目和公司的项目，尽量写的优雅些

*后续会继续集成其他框架进来*