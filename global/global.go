package global

import (
	"daocloud.io/dmp/demo/config"
	// "github.com/go-redis/redis"
	"github.com/garyburd/redigo/redis"
	"github.com/jinzhu/gorm"
)

/**
 * @author  liyuan.zhang
 * @date  2021/2/24 17:43
 */
var (
	CONFIG  config.Config
	DB      *gorm.DB
	REDISDB redis.Conn
)
