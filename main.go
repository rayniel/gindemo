package main

import (
	"daocloud.io/dmp/demo/global"
	// 这里一定要引入initialize包   用于初始化操作
	_ "daocloud.io/dmp/demo/initialize"
	"daocloud.io/dmp/demo/router"
)

/**
 * @author  liyuan.zhang
 * @date  2021/2/24 17:18
 */

func main() {
	r := router.InitRouters()
	defer global.DB.Close()
	defer global.REDISDB.Close()
	_ = r.Run() //listen and serve on 0.0.0.0:8080
}
