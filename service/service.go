package service

import (
	"daocloud.io/dmp/demo/global"
	"daocloud.io/dmp/demo/model"
)

/**
 * @author  liyuan.zhang
 * @date  2021/2/24 22:46
 */

func AddUser(user *model.User) error {
	return global.DB.Save(user).Error
}

func FindUserById(userId int, user *model.User) error {
	return global.DB.Where("id = ?", userId).Find(user).Error
}

func FindAllUsers(users *[]model.User) error {
	return global.DB.Model(model.User{}).Find(users).Error
}
